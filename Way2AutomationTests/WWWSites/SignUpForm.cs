﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Way2AutomationTests
{
    class SignUpForm : BaseForm
    {
        public SignUpForm(IWebDriver webDriver) : base(webDriver)
        {
        }

        private IWebElement _nameInput {
            get
            {
                return driver.FindElement(By.Name("name"));
            }
        }

        private IWebElement _phoneInput
        {
            get
            {
                return driver.FindElement(By.Name("phone"));
            }
        }

        private IWebElement _emailInput
        {
            get
            {
                return driver.FindElement(By.Name("email"));
            }
        }

        private IWebElement _countryInput
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/fieldset[4]/select"));
            }
        }

        private IWebElement _cityInput
        {
            get
            {
                return driver.FindElement(By.Name("city"));
            }
        }

        private IWebElement _passwordInput
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/fieldset[7]/input"));
            }
        }

        private IWebElement _userNameInput
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/fieldset[6]/input"));
            }
        }

        private IWebElement _submitSignUpButton
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/div/div[2]/input"));
            }
        }


        public void EnterUserName(string userName)
        {
            _userNameInput.Clear();
            _userNameInput.SendKeys(userName);
        }

        public void EnterPassword(string password)
        {
            _passwordInput.Clear();
            _passwordInput.SendKeys(password);
        }


        #region FormFill

        public void EnterName(string name)
        {
            _nameInput.Clear();
            _nameInput.SendKeys(name);
        }

        public void EnterPhone(string phone)
        {
            _phoneInput.Clear();
            _phoneInput.SendKeys(phone);
        }

        public void EnterEmail(string email)
        {
            _emailInput.Clear();
            _emailInput.SendKeys(email);
        }

        public void EnterCity(string city)
        {
            _cityInput.Clear();
            _cityInput.SendKeys(city);
        }

        public void ChooseCountry(string countryName)
        {
            var country = new SelectElement(_countryInput);

            country.SelectByValue(countryName);
        }

        #endregion

        public string GetToolTipText()
        {
            return _nameInput.GetAttribute("title");
        }

        public string GenerateValidEmail()
        {
            Guid guid = Guid.NewGuid();

            string mejl = guid.ToString().Substring(0,6) + "@mejl";
            return mejl;
        }

        public void ValidSignUp()
        {
            OpenSignUpForm();
            EnterName("asd");
            EnterPhone("ads");
            EnterEmail(GenerateValidEmail());
            ChooseCountry("Poland");
            EnterCity("asd");
            EnterUserName("asd");
            EnterPassword("asd");

            SubmitSignUpForm();
        }

        public void ValidSignUp(string name, string phone, string country, string city, string username, string password)
        {
            OpenSignUpForm();
            EnterName("asd");
            EnterPhone("ads");
            EnterEmail(GenerateValidEmail());
            ChooseCountry("Poland");
            EnterCity("asd");
            EnterUserName("asd");
            EnterPassword("asd");

            SubmitSignUpForm();
        }


        public void SubmitSignUpForm()
        {
            _submitSignUpButton.Click();
        }
    }
}
