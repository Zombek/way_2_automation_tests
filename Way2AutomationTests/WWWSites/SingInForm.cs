﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Way2AutomationTests.WWWSites
{
    class SingInForm : BaseForm
    {
        public SingInForm(IWebDriver webDriver) : base(webDriver)
        {
        }

        private IWebElement _userNameInput
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div[4]/div/div/div/div/div/form/fieldset[1]/input"));
            }
        }

        private IWebElement _passwordInput
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div[4]/div/div/div/div/div/form/fieldset[2]/input"));
            }
        }

        private IWebElement _submitSignInButton
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div[4]/div/div/div/div/div/form/div/div[2]/input"));
            }
        }

        private IWebElement _alert
        {
            get
            {
                return driver.FindElement(By.XPath("//*[@id='alert1']"));
            }
        }

        public void EnterUserName(string userName)
        {
            _userNameInput.Clear();
            _userNameInput.SendKeys(userName);
        }

        public void EnterPassword(string password)
        {
            _passwordInput.Clear();
            _passwordInput.SendKeys(password);
        }

        public void SubmitSignInForm()
        {
            _submitSignInButton.Click();
        }

        public bool IsAlertDisplayed()
        {
            if (_alert.Enabled) return true;
            else return false;
        }

        public void LoginAsValidUser()
        {
            OpenSignUpForm();
            NavigateToSignInForm();

            EnterUserName("asd");
            EnterPassword("asd");

            SubmitSignInForm();
        }
    }
}
