﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Way2AutomationTests
{
    class BaseForm
    {
        internal IWebDriver driver;
        private WebDriverWait _wait;

        public BaseForm(IWebDriver webDriver)
        {
            driver = webDriver;
            _wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));

        }

        internal string _formHeader
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/h3")).Text;
            }
        }

        private IWebElement _signInButton
        {
            get
            {
                return driver.FindElement(By.XPath("//html/body/div[4]/div/div/div/div/div/form/div/div[1]/p/a"));
            }
        }

        private IWebElement _singUpButton
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/div[4]/div/div/div/div/div/form/div/div[1]/p/a"));
            }
        }

        public bool CheckIfFormIsOpen()
        {
            try
            {
                IWebElement element = _wait.Until(condition: ExpectedConditions.ElementIsVisible(By.ClassName("fancybox-opened")));
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void OpenSignUpForm()
        {
            driver.Navigate().GoToUrl("http://way2automation.com/way2auto_jquery/index.php");
        }

        public void CloseSite()
        {
            driver.Close();
        }

        public void NavigateToSignInForm()
        {
            IWebElement element = _wait.Until(condition: ExpectedConditions.ElementIsVisible(By.XPath("//html/body/div[4]/div/div/div/div/div/form/div/div[1]/p/a")));
            element.Click();
        }

        public void NavigateToSignUpForm()
        {
            _singUpButton.Click();
        }

        public bool IsLoginHeaderOnForm()
        {
            string textInHeader = "LOGIN";
            if (_formHeader == textInHeader) return true;
            else return false;
        }

        public bool IsSignUpHeaderOnForm()
        {
            string textInHeader = "REGISTRATION FORM";
            if (_formHeader == textInHeader) return true;
            else return false;
        }
    }
}
