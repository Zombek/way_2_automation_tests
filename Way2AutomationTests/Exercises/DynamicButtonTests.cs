﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Way2AutomationTests.WWWSites;

namespace Way2AutomationTests.Exercises
{
    [TestClass]
    public class DynamicButtonTests
    {
        private ChromeDriver _driver;
        private WebDriverWait _wait;

        [TestInitialize]
        public void PrepareTests()
        {
            _driver = new ChromeDriver();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(50));

        }


        [TestMethod]
        //Check If dynamic button stays enabled using Xpath string function starts with
        public void CheckIfButtonStaysEnabledStartsWith()
        {
            GoToDynamicButtons();

            _driver.SwitchTo().Frame(0);
            _driver.FindElementByXPath("//input[@type = 'text']").SendKeys("test");
            _driver.FindElementByXPath("//input[@type = 'button']").Click();

            Assert.IsTrue(_driver.FindElementByXPath("//input[starts-with(@id, 'submit')]").Enabled);
        }

        [TestMethod]
        public void CheckIfButtonStaysEnabledEndsWith()
        {
            GoToDynamicButtons();

            _driver.FindElement(By.CssSelector("a[href*='tab-2']")).Click();
            _driver.SwitchTo().Frame(1);

            _driver.FindElementByXPath("//input[@type = 'text']").SendKeys("test");
            var submitButton = _driver.FindElementByXPath("//input[@type = 'button']");
            submitButton.Click();

            var elements = _driver.FindElements(By.XPath("//input"));

            //*[ends-with(@id,"1111")]
            Assert.IsTrue(elements[1].GetAttribute("Id").EndsWith("1111") && elements[1].Enabled);
        }

        [TestMethod]
        public void CheckIfButtonStaysEnabledWithCompleteDynamicID()
        {
            GoToDynamicButtons();

            _driver.FindElement(By.CssSelector("a[href*='tab-3']")).Click();
            _driver.SwitchTo().Frame(2);

            _driver.FindElementByXPath("//input[@type = 'text']").SendKeys("test");
            var submitButton = _driver.FindElementByXPath("//input[@type = 'button']");
            submitButton.Click();

            Assert.IsTrue(_driver.FindElement(By.CssSelector("input[type='button'][name='submit']")).Enabled);
        }


        [TestCleanup]
        public void CloseBrowser()
        {
            _driver.Close();
        }

        private void GoToDynamicButtons()
        {
            SingInForm singInForm = new SingInForm(_driver);
            singInForm.LoginAsValidUser();

            IWebElement element = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//h2[text()='Submit Button Clicked']")));
            element.Click();
        }

    }
}
