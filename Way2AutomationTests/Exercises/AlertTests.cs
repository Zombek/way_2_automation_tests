﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Html5;
using OpenQA.Selenium.Support.UI;

namespace Way2AutomationTests.Exercises
{
    [TestClass]
    public class AlertTests
    {
       private ChromeDriver _driver;
        private WebDriverWait _wait;
        private SignUpForm _signUpForm;

        [TestInitialize]
        public void PrepareTests()
        {
            _driver = new ChromeDriver();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(50));
            _signUpForm = new SignUpForm(_driver);

        }

        [TestMethod]
        public void AcceptSimpleAlert()
        {
            GoToAlerts();

            _driver.SwitchTo().Frame(_driver.FindElement(By.ClassName("demo-frame")));

            _driver.FindElement(By.XPath("//button[contains(text(),'Click the button to display an alert box:')]")).Click();
           
            IAlert alert = _driver.SwitchTo().Alert();
            alert.Accept();
        }

        [TestMethod]
        public void DeclineAlert()
        {
            GoToAlerts();

            _driver.FindElement(By.CssSelector("a[href*='tab-2']")).Click();
            _driver.SwitchTo().Frame(1);
            _driver.FindElement(By.TagName("button")).Click();

            IAlert alert = _driver.SwitchTo().Alert();
            alert.Dismiss();
        }

        [TestMethod]
        public void AcceptAlert()
        {
            GoToAlerts();

            _driver.FindElement(By.CssSelector("a[href*='tab-2']")).Click();
            _driver.SwitchTo().Frame(1);
            _driver.FindElement(By.TagName("button")).Click();
            IAlert alert = _driver.SwitchTo().Alert();

            string name = "Johny";
            alert.SendKeys(name);
            alert.Accept();

            string alertMessage = $"Hello {name}! How are you today?";

            Assert.AreEqual(_driver.FindElement(By.Id("demo")).Text, alertMessage);
        }

        public void GoToAlerts()
        {
            _signUpForm.ValidSignUp();

            IWebElement element = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//html/body/section/div/div[2]/div[6]/ul/li/a/h2")));

            element.Click();
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            _driver.Close();
        }
    }
}
