﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Way2AutomationTests.WWWSites;

namespace Way2AutomationTests.Exercises
{
    [TestClass]
    public class DropDownTests
    {
        private ChromeDriver _driver;
        private WebDriverWait _wait;

        [TestInitialize]
        public void PrepareTests()
        {
            _driver = new ChromeDriver();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(50));

        }


        [TestMethod]
        public void CheckTheDefaultText()
        {
            GoToDropDown();
            _driver.FindElement(By.CssSelector("a[href*='tab-1']")).Click();
            _driver.SwitchTo().Frame(0);
            var countries = new SelectElement(_driver.FindElement(By.TagName("select")));
            Assert.AreEqual("Please Select", countries.SelectedOption.Text);
            //    country.SelectByValue(countryName);
        }

        [TestMethod]
        public void CheckForTheTextOfSelectedCountry()
        {
            string toChoose = "Poland";

            GoToDropDown();
            _driver.FindElement(By.CssSelector("a[href*='tab-1']")).Click();
            _driver.SwitchTo().Frame(0);
            var countries = new SelectElement(_driver.FindElement(By.TagName("select")));

            countries.SelectByValue(toChoose);

            Assert.AreEqual(toChoose, countries.SelectedOption.Text);
        }

        [TestMethod]
        public void CheckForTheSugestedCountriesInDynamic()
        {
            string toChoose = "Poland";
            string toSearch = "pol";

            GoToDropDown();
            _driver.FindElement(By.CssSelector("a[href*='tab-2']")).Click();
            _driver.SwitchTo().Frame(1);

            IWebElement input = _driver.FindElement(By.CssSelector("input"));
            input.Clear();
            input.SendKeys(toSearch);

            var countryList = _driver.FindElements(By.CssSelector(".ui-menu-item"));

            _wait.Until(ExpectedConditions.TextToBePresentInElement(countryList[2], toChoose));

            //check if the elements have the same substring
            foreach (var country in countryList)
            {
                string optionToChoose = country.GetAttribute("textContent").ToLower();

                Console.WriteLine(optionToChoose);

                Assert.IsTrue(optionToChoose.Contains(toSearch));
            }
        }

        [TestMethod]
        public void ClickSugestedOption()
        {
            string toChoose = "Poland";

            GoToDropDown();
            _driver.FindElement(By.CssSelector("a[href*='tab-2']")).Click();
            _driver.SwitchTo().Frame(1);

            IWebElement input = _driver.FindElement(By.CssSelector("input"));
            input.Clear();
            input.SendKeys(toChoose);

            var countryList = _driver.FindElements(By.CssSelector(".ui-menu-item"));

            _wait.Until(ExpectedConditions.TextToBePresentInElement(countryList[2], toChoose));

            foreach (var country in countryList)
            {
                string optionToChoose = country.GetAttribute("textContent");
                if (optionToChoose == toChoose)
                {
                    Console.WriteLine(optionToChoose);
                    country.Click();
                    break;
                }
            }

            //TODO: Get the new input

            var countryInput = _driver.FindElement(By.CssSelector("span > input"));
         //   _wait.Until(ExpectedConditions.TextToBePresentInElement(countryInput, toChoose));

            Assert.AreEqual(toChoose, countryInput.Text);      
        }
 

        [TestCleanup]
        public void CloseBrowser()
        {
            _driver.Close();
        }

        private void GoToDropDown()
        {
            SingInForm singInForm = new SingInForm(_driver);
            singInForm.LoginAsValidUser();

            IWebElement element = _wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//h2[text()='Dropdown']")));
            element.Click();
        }
    }
}
