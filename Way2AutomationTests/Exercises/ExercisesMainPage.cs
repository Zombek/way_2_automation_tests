﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Way2AutomationTests.Exercises
{
    [TestClass]
    public class ExercisesMainPage
    {
        IWebDriver _driver;

         [TestInitialize]
        public void PrepareTests()
        {
            _driver = new ChromeDriver();

            _driver.Navigate().GoToUrl("http://way2automation.com/way2auto_jquery/index.php");
        }

        [TestMethod]
        public void CountAllElements()
        {
          var elements = _driver.FindElements(By.TagName("figure"));

            Console.WriteLine(elements.Count);
        }

        [TestMethod]
        public void CountAllElementsInWidget()
        {
            var element = _driver.FindElement(By.XPath("//html/body/section/div[2]/div[2]/div[2]/ul"));

            var elements = element.FindElements(By.TagName("li"));

            Console.WriteLine(elements.Count);
        }


        [TestMethod]
        public void ClickDropDownWidget()
        {
            var  _signUpForm = new SignUpForm(_driver);

            _signUpForm.ValidSignUp();
            
            Thread.Sleep(10000);

            var element = _driver.FindElement(By.XPath("//html/body/section/div[2]/div[2]/div[4]/ul/li[2]/a/h2"));

            element.Click();
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            _driver.Close();
        }
    }
}
