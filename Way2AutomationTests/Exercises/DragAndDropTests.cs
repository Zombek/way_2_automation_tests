﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace Way2AutomationTests
{
    [TestClass]
    public class DragAndDropTests
    {
        private ChromeDriver _driver;
        private string _url = "http://www.dhtmlgoodies.com/scripts/drag-drop-custom/demo-drag-drop-3.html";
        private WebDriverWait _wait;

        [TestInitialize]
        public void PrepareTests()
        {
            _driver = new ChromeDriver();
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(50));
        }


        [TestMethod]
        public void DraggAndDropCorrectCityToCountry()
        {
            IWebElement drag;
            IWebElement dropArea;
            List<DragAndDropMe> dragAndDropMes = new List<DragAndDropMe>();

            GoToURL();

            //Get all DragAndDrop Items
            for (int i = 1; i < 8; i++)
            {
                string dragPath = $"//*[@id='box{i}']";
                string dropPath = $"//*[@id='box10{i}']";

                drag = _driver.FindElement(By.XPath(dragPath));
                dropArea = _driver.FindElement(By.XPath(dropPath));

                dragAndDropMes.Add(new DragAndDropMe(drag, dropArea));
            }

            foreach (var item in dragAndDropMes)
            {
                //check for the CCS value till the timeout occurs
                _wait.Until(d =>
                {
                    if (item.drag.GetCssValue("background-color") == "rgba(0, 255, 0, 1)")
                    {
                        return item.drag;
                    }
                    PerformDragAndDrop(item);
                    return null;
                });

                //Assert the dragged item is green
                string actualValue = item.drag.GetCssValue("background-color");
                string expectedValue = "rgba(0, 255, 0, 1)";
                Assert.AreEqual(expectedValue, actualValue);
            }
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            _driver.Close();
        }

        private void GoToURL()
        {
            _driver.Navigate().GoToUrl(_url);
        }

        private void PerformDragAndDrop(DragAndDropMe dragAndDropMe)
        {
            Actions action = new Actions(_driver);
            action.DragAndDrop(dragAndDropMe.drag, dragAndDropMe.dropArea).Perform();
        }

        private class DragAndDropMe
        {
            public IWebElement drag { get; set; }
            public IWebElement dropArea { get; set; }

            public DragAndDropMe(IWebElement dragMe, IWebElement drop)
            {
                drag = dragMe;
                dropArea = drop;
            }
        }
    }

}
