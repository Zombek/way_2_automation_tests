﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Way2AutomationTests
{
    [TestClass]
    public class SignUpFormTests
    {
        private SignUpForm _signUpForm;

        private string _name = "Test Name";
        private string _phone = "712546325";
        private string _validMail = "imValid@mail";
        private string _halfValidMail = "imValid@";
        private string _notValidMail = "imnNotValid";
        private string _city = "NY";
        private string _userName = "UserName";
        private string _password = "superFancyPassword";


        //TODO Get The Tooltip
        #region InvalidSignUps
        [TestInitialize]
        public void PrepareTests()
        {
            var driver = new ChromeDriver();

            _signUpForm = new SignUpForm(driver);
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out any field

        public void InvalidSignUpWithNoData()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.SubmitSignUpForm();

            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());

            Console.WriteLine(_signUpForm.GetToolTipText());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyName()
        {
           _signUpForm.OpenSignUpForm();
           _signUpForm.EnterName(_name);
           _signUpForm.SubmitSignUpForm();

            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithNameAndPhone()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithNamePhoneInvalidMail()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_notValidMail);
     
            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyNamePhoneInvalidMail()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_notValidMail);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyNamePhonePartialyInvalidMail()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_halfValidMail);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyNamePhonePartialyInvalidMailCountry()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_halfValidMail);
            _signUpForm.ChooseCountry("Poland");

            _signUpForm.SubmitSignUpForm();

            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyNamePhonePartialyInvalidMailCountryCity()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_halfValidMail);
            _signUpForm.ChooseCountry("Poland");
            _signUpForm.EnterCity(_city);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields

        public void InvalidSignUpWithOnlyNamePhonePartialyInvalidMailCountryCityUser()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_halfValidMail);
            _signUpForm.ChooseCountry("Poland");
            _signUpForm.EnterCity(_city);
            _signUpForm.EnterUserName(_userName);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup without filling out all fields
        public void InvalidSignUpWithOnlyNamePhonePartialyInvalidMailCountryCityUserPassword()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_halfValidMail);
            _signUpForm.ChooseCountry("Poland");
            _signUpForm.EnterCity(_city);
            _signUpForm.EnterUserName(_userName);
            _signUpForm.EnterPassword(_password);

            _signUpForm.SubmitSignUpForm();
            Assert.IsTrue(_signUpForm.IsSignUpHeaderOnForm());
        }


        [TestMethod]
        [TestCategory("Signup")]
        //Check if you can signup as a before created user
        public void InalidSignUpWitExistingUser()
        {
            _signUpForm.OpenSignUpForm();
            _signUpForm.EnterName(_name);
            _signUpForm.EnterPhone(_phone);
            _signUpForm.EnterEmail(_validMail);
            _signUpForm.ChooseCountry("Poland");
            _signUpForm.EnterCity(_city);
            _signUpForm.EnterUserName(_userName);
            _signUpForm.EnterPassword(_password);

            _signUpForm.SubmitSignUpForm();

            Assert.IsTrue(_signUpForm.CheckIfFormIsOpen());
        }
        #endregion


        [TestMethod]
        [TestCategory("Signup")]
        //Ckech if you can do a valid signup
        public void ValidSignUp()
        {
            _signUpForm.ValidSignUp(_name, _phone, "Poland", _city, _userName, _password);

             Assert.IsFalse(_signUpForm.CheckIfFormIsOpen());
        }

        [TestMethod]
        [TestCategory("Navgigation")]
        //Check if the sing in button works
        public void NavigateToSignInForm()
        {
            _signUpForm.OpenSignUpForm();

            _signUpForm.NavigateToSignInForm();
            Assert.IsTrue(_signUpForm.IsLoginHeaderOnForm());
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            _signUpForm.CloseSite();
        }
    }
}
