﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using Way2AutomationTests.WWWSites;

namespace Way2AutomationTests.Exercises
{
    [TestClass]
    public class SignInFormTests
    {
        private SingInForm _singInForm;

        //TODO Get The Tooltip

        [TestInitialize]
        public void PrepareTests()
        {
            var driver = new ChromeDriver();

            _singInForm = new SingInForm(driver);
        }

        [TestMethod]

        //Check If a non existing user can log in
        public void UnvalidSingInWithAllData()
        {
            _singInForm.OpenSignUpForm();
            _singInForm.NavigateToSignInForm();

            _singInForm.EnterUserName("213565312645wd3sgr");
            _singInForm.EnterPassword("213565312645wd3sgr");

            _singInForm.SubmitSignInForm();

            Assert.IsTrue(_singInForm.IsAlertDisplayed());
        }

        //Check If you can log in with only a username
        [TestMethod]
        public void UnvalidSingInWithOnlyUserName()
        {
            _singInForm.OpenSignUpForm();
            _singInForm.NavigateToSignInForm();

            _singInForm.EnterUserName("asd");
            _singInForm.SubmitSignInForm();

            Assert.IsTrue(_singInForm.IsLoginHeaderOnForm());
        }

        [TestMethod]
        //Check If you can log in with only a password

        public void UnvalidSingInWithOnlyPassword()
        {
            _singInForm.OpenSignUpForm();
            _singInForm.NavigateToSignInForm();

            _singInForm.EnterPassword("asd");

            _singInForm.SubmitSignInForm();

            Assert.IsTrue(_singInForm.IsLoginHeaderOnForm());
        }

        [TestMethod]
        //Check if you cal log in with no data
        public void UnvalidSingInWithNoData()
        {
            _singInForm.OpenSignUpForm();
            _singInForm.NavigateToSignInForm();

            _singInForm.SubmitSignInForm();

            Assert.IsTrue(_singInForm.IsLoginHeaderOnForm());
        }

        [TestMethod]
        //Check navigation between forms
        public void NavigateBackToSingUp()
        {
            _singInForm.OpenSignUpForm();
            _singInForm.NavigateToSignInForm();
            _singInForm.NavigateToSignUpForm();

            Assert.IsTrue(_singInForm.IsSignUpHeaderOnForm());
        }

        [TestMethod]
        //A valid signup, with a previous created user
        public void ValidSignUp()
        {
            _singInForm.LoginAsValidUser();

            Assert.IsFalse(_singInForm.CheckIfFormIsOpen());
        }

        [TestCleanup]
        public void CloseBrowser()
        {
            _singInForm.CloseSite();
        }
    }
}
